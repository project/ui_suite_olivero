sp_replacements = {
  'var(--sp0-25)': 'var(--sp-)',
  'var(--sp0-5)': 'var(--sp-)',
  'var(--sp0-75)': 'var(--sp-)',
  'var(--sp1)': 'var(--sp-m)',
  'var(--sp1-5)': 'var(--sp-l)',
  'var(--sp2)': 'var(--sp-xl)',
  'var(--sp2-5)': 'var(--sp-xl)',
  'var(--sp3)': 'var(--sp-2xl)',
  'var(--sp3-5)': 'var(--sp-)',
  'var(--sp4)': 'var(--sp-)',
  'var(--sp4-5)': 'var(--sp-)',
  'var(--sp5)': 'var(--sp-)',
  'var(--sp6)': 'var(--sp-)',
  'var(--sp7)': 'var(--sp-)',
  'var(--sp8)': 'var(--sp-)',
  'var(--sp9)': 'var(--sp-)',
  'var(--sp10)': 'var(--sp-)',
  'var(--sp11)': 'var(--sp-)',
  'var(--sp12)': 'var(--sp-)',
  'var(--sp13)': 'var(--sp-)',
}

query_replacements = {
  '(--sm)': '#{$mq__sm}'
  '(--md)': '#{$mq__md}'
  '(--lg)': '#{$mq__lg}'
  '(--xl)': '#{$mq__xl}'
  '(--nav-md)': '#{$mq__nav_md}'
  '(--nav)': '#{$mq__nav}'
  '(--max-nav)': '#{$mq__max_nav}'
}

css_files = '/var/www/rollgut/web/core/themes/olivero/css'

for path in Path(css_files).rglob('*.css'):
  with(open(path, 'rw')) as f:
    file_content = file.read()

    for old_str, new_str in sp_replacements.items():
        file_content = file_content.replace(old_str, new_str)

    for old_str, new_str in query_replacements.items():
        file_content = file_content.replace(old_str, new_str)

    f.write(file_content)
