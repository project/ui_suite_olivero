Every css variable within this Folder can be redefined using ui_skins.

:root is the default scope, but others can be chosen as well.
This way, you f.e. specify the font for blocks title!
Simple configuring your ui_skins variables in the Backend enables a lot of power.
